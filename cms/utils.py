
from collections import defaultdict
from typing import Final

import numpy as np
import datetime
import calendar


def take_average_of_same_datetime_objects(objects):
    print('-------------------------------------------------')
    print('take_average_of_same_datetime_objects')
    print('in length: ', len(objects))
    res = defaultdict(list)
    for v in objects:
        # print(v)
        res[v['Date']].append(v)

    output = []
    for k, v in res.items():
        new_obj = {}
        new_obj['Date'] = k
        new_obj['Close'] = sum([x['Close'] for x in v]) / len(v)
        output.append(new_obj)
    print('diff', list(set([str(x)
                            for x in objects]) - set([str(x) for x in output])))
    print('out length: ', len(output))

    print('-------------------------------------------------')
    return output


def format_objects_to_regular_intervals(objects, interval='monthly'):

    for object in objects:
        object['_date'] = object['Date']

    print(request.data['item'])
    objects = SalesData.objects.filter(
        item=request.data['item']).order_by('date_time').values()


def create_dataset(dataset, look_back=15, max_predict_period=1):
    print('-'*50)
    print('create_dataset')
    dataX, dataY = [], []
    for i in range(len(dataset)-look_back-max_predict_period):
        a = dataset[i:(i+look_back), 0]
        dataX.append(a)
        dataY.append(dataset[i + look_back + max_predict_period - 1, 0])
    print('-'*50)
    return np.array(dataX), np.array(dataY)


def create_predictset(dataset, look_back=15, max_predict_period=1, predict_period=1):
    print('-'*50)
    print('create_predictset')
    print('dataset tail \n', dataset)
    print(" ")
    dataX, dataY = [], []
    for i in range(predict_period):
        dataset_start_index = len(
            dataset) - (predict_period - i) - look_back + 1
        a = dataset[dataset_start_index: dataset_start_index + look_back, 0]
        print('predict set ', i, ": \n", a)
        dataX.append(a)
    print('-'*50)

    return np.array(dataX)


def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year, month)[1])

    return datetime.datetime(year, month, day)


def get_predicted_data_headers(predict_period, headers):
    final_date = headers[-1]
    predict_headers = []
    for i in range(predict_period):
        print(final_date, " ", i)
        predict_headers.append(add_months(final_date, i + 1))

    return predict_headers


def getModelSavedName(item, predictField):
    return 'app/predictModels/{item}---{predictField}.h5'.format(
        item=item, predictField=predictField)


def getModelWeightsSavedName(item, predictField):
    return 'app/predictModels/{item}---{predictField}---{model}.h5'.format(
        item=item, predictField=predictField, model='weights')
