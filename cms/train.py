from os import name
from django.forms.utils import flatatt
from keras.layers import LSTM, Dropout, Dense
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from cms.utils import take_average_of_same_datetime_objects, create_dataset, create_predictset, get_predicted_data_headers, getModelSavedName, getModelWeightsSavedName
from django.db.models.expressions import F


from django.shortcuts import render
from django.contrib import messages
from numpy.core.fromnumeric import take
from .models import SalesData, SupermarketSale, TestModel

import math

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# %matplotlib inline
from matplotlib.pylab import rcParams
rcParams['figure.figsize'] = 20, 10


def train(predictField='price', item=None):
    if(item):
        items = [item]
    else:
        items = SalesData.objects.values_list('item', flat=True)

    items = list(set(items))
    statuses = []

    max_predict_period = 6
    look_back = 15
    train_split_ratio = 0.6

    print(items)
    for item in items:

        status = {'item': item}

        try:

            print(item)
            raw_objects = SalesData.objects.filter(
                item=item).order_by('date_time').values()

            objects = []

            ##########  Data preparation ##############################################################################################
            for raw_object in raw_objects:
                object = {}
                object['Date'] = raw_object['date_time']
                object['Close'] = raw_object[predictField]
                objects.append(object)

            objects = take_average_of_same_datetime_objects(objects)

            # TODO add time interval maker

            data_df = pd.DataFrame.from_records(objects)

            print(data_df.head())

            data_df = data_df[['Date', 'Close']]

            data_df = data_df.replace({'\$': ''}, regex=True)

            data_df = data_df.astype({"Close": float})

            data_df.dtypes

            data_df.index = data_df['Date']

            min_max_scaler = MinMaxScaler(feature_range=(0, 1))
            dataset = min_max_scaler.fit_transform(
                data_df['Close'].values.reshape(-1, 1))

            print('-------------------------------------------------')
            print('dataset \n', dataset)

            train_size = int(len(dataset) * train_split_ratio)
            test_size = len(dataset) - train_size

            status['dataset_size'] = len(dataset)
            status['train_size'] = train_size
            status['test_size'] = test_size

            if(train_size < look_back or test_size < look_back):
                status['error'] = 'Not enough data for the model to be trained'
                status['success'] = False

            train, test = dataset[0:train_size,
                                  :], dataset[train_size:len(dataset), :]
            print('-------------------------------------------------')
            print(len(train), len(test), ' len(train), len(test)')
            print('-------------------------------------------------')

            x_train, y_train = create_dataset(
                train, look_back=look_back, max_predict_period=max_predict_period)
            x_test, y_test = create_dataset(
                test, look_back=look_back, max_predict_period=max_predict_period)

            print('-------------------------------------------------')
            print(x_train.shape, ' x_train.shape')
            print(' ')
            print(y_train.shape, ' y_train.shape')
            print(' ')
            print(x_test.shape, ' x_test.shape')
            print(' ')
            print(y_test.shape, ' y_test.shape')
            print('-------------------------------------------------')

            x_train = np.reshape(
                x_train, (x_train.shape[0], 1, x_train.shape[1]))
            x_test = np.reshape(x_test, (x_test.shape[0], 1, x_test.shape[1]))

            # print('x test ', x_test)
            # print('y train ', y_train)
            print('-------------------------------------------------')

            ###########  Model preparation  #############################################################################################

            model = Sequential()
            model.add(LSTM(20, input_shape=(1, look_back)))
            model.add(Dense(1))
            model.compile(loss='mean_squared_error', optimizer='adam')

            ##########  Training data  ##############################################################################################
            model.fit(x_train, y_train, epochs=20, batch_size=1, verbose=2)

            ###########  Testing the trained model #############################################################################################
            trainPredict = model.predict(x_train)
            testPredict = model.predict(x_test)

            one_line = np.array([x_train[0]])
            one_line_predict = model.predict(one_line)
            print(one_line_predict, '\n oneline \n', one_line)

            # invert predictions
            trainPredict = min_max_scaler.inverse_transform(trainPredict)
            trainY = min_max_scaler.inverse_transform([y_train])
            testPredict = min_max_scaler.inverse_transform(testPredict)
            testY = min_max_scaler.inverse_transform([y_test])

            # calculate root mean squared error
            trainScore = math.sqrt(mean_squared_error(
                trainY[0], trainPredict[:, 0]))
            print('Train Score: %.2f RMSE' % (trainScore))
            status['train_score'] = 'Train Score: %.2f RMSE' % (trainScore)

            testScore = math.sqrt(mean_squared_error(
                testY[0], testPredict[:, 0]))
            print('Test Score: %.2f RMSE' % (testScore))
            status['test_score'] = 'Test Score: %.2f RMSE' % (testScore)

            model.summary()

            ###########  Saving model  #############################################################################################
            model_name = getModelSavedName(item, predictField)
            model_weights_name = getModelWeightsSavedName(item, predictField)
            
            model.save(model_name,
                       save_traces=True,
                       overwrite=True,
                       include_optimizer=True,)

            model.save_weights(model_weights_name)


            status['success'] = True
        except:
            status['success'] = False
            status['error'] = 'unknown error'

        statuses.append(status)

    data = {
        'success': True,
        'statuses': statuses,
        'lookback': look_back,
        'max_predict_period': max_predict_period}

    return data


  
