from django.db import models
from django.db.models.fields import DateTimeField
class SupermarketSale(models.Model):

    gender_choices = [
        ('Male', 'Male'),
        ('Female', 'Female'),

    ]

    customer_type = [
        ('Normal', 'Normal'),
        ('Member', 'Member')
    ]

    id = models.AutoField(primary_key=True)
    invoice_id = models.CharField(null=True, blank=True, max_length=50)
    branch = models.CharField(null=True, blank=True, max_length=50)
    city = models.CharField(null=True, blank=True, max_length=50)
    customer_type = models.CharField(choices=gender_choices, max_length=100, null=True, blank=True)
    gender = models.CharField(choices=gender_choices, max_length=100, null=True, blank=True)
    product_line = models.CharField(null=True, blank=True, max_length=50, )
    unit_price = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)
    quantity = models.DecimalField(max_digits=12, decimal_places=0, null=True, blank=True)
    tax_percentage = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True)
    date = models.DateField(blank=True, null=True)
    time = models.TimeField(blank=True, null=True)
    payment_type = models.CharField(null=True, blank=True, max_length=50)
    cogs = models.CharField(null=True, blank=True, max_length=50)
    gross_margin_percentage = models.DecimalField(
        max_digits=20, decimal_places=10, null=True, blank=True)
    gross_income = models.DecimalField(max_digits=20, decimal_places=10, null=True, blank=True)
    rating = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True)

class SalesData(models.Model):
    id = models.AutoField(primary_key=True)
    item = models.CharField(max_length=50)
    category = models.CharField(max_length=50)
    date_time = models.DateTimeField()
    price = models.DecimalField(max_digits=20, decimal_places=3, null=True, blank=True)
    demand = models.DecimalField(max_digits=20, decimal_places=2, null=True, blank=True)

class TestModel(models.Model):
    name = models.CharField(max_length=150)
    email = models.EmailField(blank=True)
    address = models.CharField(max_length=50)
    phone = models.CharField(max_length=150,unique=True)
    profile = models.TextField()
    def __str__(self):
        return self.name