from keras.layers import LSTM, Dropout, Dense
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential, load_model

from cms.utils import take_average_of_same_datetime_objects, create_dataset, create_predictset, get_predicted_data_headers, getModelSavedName, getModelWeightsSavedName
from django.db.models.expressions import F
from django.shortcuts import render

from cms.train import train

# Create your views here.

import csv
import io
from django.shortcuts import render
from numpy.core.fromnumeric import take
from .models import SalesData
from datetime import datetime
from rest_framework.permissions import IsAuthenticated
from .serializers import SalesDataSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action

import math
import time

import pandas as pd
import numpy as np
class SalesDataViewset(viewsets.ModelViewSet):
    queryset = SalesData.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = SalesDataSerializer

    #Csv upload handling action
    @action(detail=False, methods=['post'])
    def uploadCsv(self, request, *args, **kwargs):
        try:
            data = request.data
            print(data)
            formattedData = [SalesData(price=float(x['Price'].replace('"', "")), demand=float(x['Demand'].replace(
                '"', "")), item=x['Item'], category=x['Category'], date_time=datetime(year=int(x['Year']), month=int(x['Month']), day=int(x['Date']) | 1)) for x in data]
            SalesData.objects.bulk_create(formattedData)
            return Response({'success': True})
        except:
            error = 'unknown error'
            return Response({'success': False, 'error': error})

    #Prediction action
    @action(detail=False, methods=['post'])
    def predict(self, request, *args, **kwargs):

        max_predict_period = 6
        look_back = 15

        predict_period = 6
        item = request.data['item']

        ##########  Data preparation  ##############################################################################################

        if(predict_period > max_predict_period):
            return Response({'success': False, 'error': "Maximum predict period exceeded. Please use a lower predict period value"})

        print(request.data['item'])
        raw_objects = SalesData.objects.filter(
            item=item).order_by('date_time').values()

        predictField = request.data['predictField']

        objects = []

        for raw_object in raw_objects:
            object = {}
            object['Date'] = raw_object['date_time']
            object['Close'] = raw_object[predictField]
            objects.append(object)

        objects = take_average_of_same_datetime_objects(objects)

        # TODO add time interval maker

        data_df = pd.DataFrame.from_records(objects)

        print(data_df.head())

        data_df = data_df[['Date', 'Close']]

        data_df = data_df.replace({'\$': ''}, regex=True)

        data_df = data_df.astype({"Close": float})
        # data_df["Date"] = pd.to_datetime(data_df.Date, format="%m/%d/%Y")
        # data_df["Date"] = pd.to_datetime(data_df.Date, format="%Y-%m-%d")

        #
        #  format="%Y-%m-%d"
        print(data_df.dtypes)

        data_df.index = data_df['Date']

        min_max_scaler = MinMaxScaler(feature_range=(0, 1))
        dataset = min_max_scaler.fit_transform(
            data_df['Close'].values.reshape(-1, 1))

        # print('-------------------------------------------------')
        # print('dataset \n', dataset)

        train_size = int(len(dataset))
        test_size = len(dataset) - train_size
        train = dataset[0:train_size, :]
        # print('-------------------------------------------------')
        # print(len(train), ' len(train), len(test)')
        # print('-------------------------------------------------')

        x_train, y_train = create_dataset(
            train, look_back=look_back, max_predict_period=max_predict_period)
        x_predict = create_predictset(
            train, look_back=look_back, max_predict_period=max_predict_period, predict_period=predict_period)

        # print("n ", len(y_train), ' l ', look_back, ' p ', max_predict_period)

        # print('-------------------------------------------------')
        # print(x_train.shape, ' x_train.shape')
        # print(' ')
        # print(y_train.shape, ' y_train.shape')

        # print('-------------------------------------------------')

        x_train = np.reshape(x_train, (x_train.shape[0], 1, x_train.shape[1]))
        x_predict = np.reshape(
            x_predict, (x_predict.shape[0], 1, x_predict.shape[1]))

        print('y train ', y_train)
        print('-------------------------------------------------')

        model_name = getModelSavedName(item, predictField)
        model_weights_name = getModelWeightsSavedName(item, predictField)


        ###########  Prediction #############################################################################################

        print(model_name)
        model = load_model(model_name, compile=True)
        model.load_weights(model_weights_name)
        model.summary()

        print("\n predict ", x_predict)

        predicted = model.predict(x_predict)
        print('predicted \n', predicted)

        one_line = np.array([x_train[0]])
        one_line_predict = model.predict(one_line)
        print(one_line_predict, '\n oneline \n', one_line)
        print("x train\n", x_train)

        # invert predictions
        predicted = min_max_scaler.inverse_transform(predicted)

        headers = [x['Date'] for x in objects]
        predicted_headers = get_predicted_data_headers(
            predict_period=predict_period, headers=headers)
        print("predicted headers ", predicted_headers)

        data = {
            'success': True,
            'data': min_max_scaler.inverse_transform(dataset),
            'predictData': predicted,
            'headers': headers,
            'predictedHeaders': predicted_headers
        }
        return Response(data)

    #Price data train action
    @action(detail=False, methods=['get', 'post'])
    def train_prices(self, request, *args, **kwargs):
        resp = train()
        return Response(resp)

    #Demand data train action
    @action(detail=False, methods=['get', 'post'])
    def train_demands(self, request, *args, **kwargs):
        resp = train(predictField="demand")
        return Response(resp)
