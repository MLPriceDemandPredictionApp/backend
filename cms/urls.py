from django.urls import path, include, re_path
from . import views

from rest_framework import routers

router = routers.DefaultRouter()

# router.register(r'supermarketSale', views.SupermarketSaleViewset)
router.register(r'salesData', views.SalesDataViewset)

urlpatterns = [
    # path('custom_page/', views.custom_page),
    
    path('', include(router.urls)),
    # path('home/', views.home, name='home'),
    # path('result/', views.result, name='result'),
   
]
