from django.contrib import admin

# Register your models here.
from .models import SupermarketSale, TestModel



@admin.register(SupermarketSale)
class SupermarketSale(admin.ModelAdmin):
    date_heirarchy = (
        'modified',
    )
    list_display = [field.name for field in SupermarketSale._meta.get_fields()]
    ordering = ("-date", 'time')   


    
admin.site.register(TestModel)
