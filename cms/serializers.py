from rest_framework import serializers 
from .models import SalesData, SupermarketSale
 
 
# class SupermarketSaleSerializer(serializers.ModelSerializer):
 
#     class Meta:
#         model = SupermarketSale
#         fields = '__all__'


class SalesDataSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = SalesData
        fields = '__all__'